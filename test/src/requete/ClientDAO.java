package requete;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import objets.Client;

public class ClientDAO {
	
	private static Connection connection;
	
	public ClientDAO(Connection connection) {
		ClientDAO.connection = connection;
	}
	
	/*
	 * Selection
	 */
	public String selectAll() {
		String result = "";
		try {
			
			PreparedStatement request = ClientDAO.connection.prepareStatement("SELECT * FROM Client");
			
			ResultSet res = request.executeQuery();
			
			while(res.next()) {
				result += "- Client " + res.getInt("id_client") + ": \n\t Nom Pr�nom : " + res.getString("nom") + " " + res.getString("prenom");
				result += "\n\t Adresse : " + res.getString("no_rue") + " " + res.getString("voie") + " " + res.getString("code_postal") + " " + res.getString("ville") + " " + res.getString("pays") + "\n\n";
			}
			res.close();
			
			if (request != null) {
				request.close();
			}
			
			return result;
		} catch(SQLException error) {
			System.out.println("Erreur selection Client : " + error.getMessage());
		}
		return "Erreur";
	}
	
	/**
	 * Cr�ation
	 */
	public static void create(Client Client) {
		try {
			PreparedStatement request = connection.prepareStatement("INSERT INTO Client (nom, prenom, " +
											"no_rue, voie, code_postal, ville, pays) VALUES (?, ?, ?, ?, ?, ?, ?)");
			
			request.setString(1, Client.getNom());
			request.setString(2, Client.getPrenom());
			request.setString(3, Client.getNo_rue());
			request.setString(4, Client.getVoie());
			request.setString(5, Client.getCode_postal());
			request.setString(6, Client.getVille());
			request.setString(7, Client.getPays());
			request.executeUpdate();
			
			if (request != null) {
				request.close();
			}
		} catch (SQLException error) {
			System.out.println("Erreur cr�ation Client : " + error.getMessage());
		}
	}
	
	/*
	 * Suppression
	 */
	public static void delete(Client Client) {
		try {
			PreparedStatement request = connection.prepareStatement("DELETE FROM Client WHERE id_client = ?");
			request.setInt(1, Client.getId_client());
			request.executeUpdate();
			
			
			if (request != null) {
				request.close();
			}
		} catch(SQLException error) {
			System.out.println("Erreur suppression Client : " + error.getMessage());
		}
	}
	
	/*
	 * Edition
	 */
	public static void edit(Client Client) {
		try {
		
			PreparedStatement request = connection.prepareStatement("UPDATE Client SET nom = ?, prenom = ?, " +
					"no_rue = ?, voie = ?, code_postal = ?, ville = ?, pays = ? WHERE id_client = ?");

			request.setString(1, Client.getNom());
			request.setString(2, Client.getPrenom());
			request.setString(3, Client.getNo_rue());
			request.setString(4, Client.getVoie());
			request.setString(5, Client.getCode_postal());
			request.setString(6, Client.getVille());
			request.setString(7, Client.getPays());
			request.setInt(8, Client.getId_client());
			
			request.executeUpdate();
		
			
			if (request != null) {
				request.close();
			}
		} catch(SQLException error) {
			System.out.println("Erreur modification Client : " + error.getMessage());
		}
	}

}
