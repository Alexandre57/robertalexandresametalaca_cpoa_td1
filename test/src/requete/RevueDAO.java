package requete;

import java.sql.*;

import objets.Revue;

public class RevueDAO {
	
	private static Connection connection;
	
	public RevueDAO(Connection connection) {
		RevueDAO.connection = connection;
	}
	
	/*
	 * Selection
	 */
	public String selectAll() {
		String result = "";
		try {
			
			PreparedStatement request = RevueDAO.connection.prepareStatement("SELECT * FROM Revue");
			ResultSet res = request.executeQuery();
			
			while(res.next()) {
				result += "- Revue " + res.getInt("id_revue") + ": \n\t Titre : " + res.getString("titre") + "\n\t Description : " + res.getString("description");
				result += "\n\t tarif_numero : " + res.getFloat("tarif_numero") + "\n\t Visuel : " + res.getString("visuel") + "\n\t ID Periodicite : " + res.getInt("id_periodicite") + "\n";
			}
			res.close();
			
			if (request != null) {
				request.close();
			}
			
			return result;
		} catch(SQLException error) {
			System.out.println("Erreur selection Revue : " + error.getMessage());
		}
		return "Erreur";
	}
	
	/**
	 * Cr�ation
	 */
	public static void create(Revue Revue) {
		try {
			PreparedStatement request = connection.prepareStatement("INSERT INTO Revue (titre, description, " +
											"tarif_numero, visuel, id_periodicite) VALUES (?, ?, ?, ?, ?)");
			
			request.setString(1, Revue.getTitre());
			request.setString(2, Revue.getDescription());
			request.setDouble(3, Revue.getTarif_numero());
			request.setString(4, Revue.getVisuel());
			request.setInt(5, Revue.getId_revue());
			
			request.executeUpdate();
			
		} catch (SQLException error) {
			System.out.println("Erreur cr�ation Revue : " + error.getMessage());
		}
	}
	
	/*
	 * Suppression
	 */
	public static void delete(Revue Revue) {
		try {
			PreparedStatement request = connection.prepareStatement("DELETE FROM Revue WHERE id_revue = ?");
			request.setInt(1, Revue.getId_revue());
			request.executeUpdate();
			
		} catch(SQLException error) {
			System.out.println("Erreur suppression Revue : " + error.getMessage());
		}
	}
	
	/*
	 * Edition
	 */
	public static void edit(Revue Revue) {
		try {
		
			PreparedStatement request = connection.prepareStatement("UPDATE Revue SET titre = ? " +
									", description = ?, tarif_numero = ?, visuel = ?, id_periodicite = ? WHERE id_revue = ?");
			
			request.setString(1, Revue.getTitre());
			request.setString(2, Revue.getDescription());
			request.setDouble(3, Revue.getTarif_numero());
			request.setString(4, Revue.getVisuel());
			request.setInt(5, Revue.getId_periodicite());
			request.setInt(6, Revue.getId_revue());
			
			request.executeUpdate();
		
		} catch(SQLException error) {
			System.out.println("Erreur modification Revue : " + error.getMessage());
		}
	}

}
