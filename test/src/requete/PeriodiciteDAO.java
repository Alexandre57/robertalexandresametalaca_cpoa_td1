package requete;

import java.sql.SQLException;
import java.sql.Statement;

import objets.Periodicite;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;

public class PeriodiciteDAO {
	
	private Connection connection;
	
	public PeriodiciteDAO(Connection connection) {
		this.connection = connection;
	}
	
	/*
	 * Selection
	 */
	public String selectAll() {
		String result = "";
		try {
			
			PreparedStatement request = this.connection.prepareStatement("SELECT * FROM Periodicite");
			ResultSet res = request.executeQuery();
			
			while(res.next()) {
				result += "- Periodicite " + res.getInt("id_periodicite") + " \t Libelle : " + res.getString("libelle") + "\n"; 
			}
			
			res.close();
			
			if (request != null) {
				request.close();
			}
			
			return result;
		} catch(SQLException error) {
			System.out.println("Erreur selection Periodicite : " + error.getMessage());
		}
		return "Erreur";
	}
	
	/**
	 * Cr�ation
	 */
	public void create(Periodicite Periodicite) {
		try {
			PreparedStatement request = this.connection.prepareStatement("INSERT INTO Periodicite (libelle) values (?)", Statement.RETURN_GENERATED_KEYS);
			request.setString(1, Periodicite.getLibelle());
			request.executeUpdate();
			
		} catch (SQLException error) {
			System.out.println("Erreur cr�ation Periodicite : " + error.getMessage());
		}
	}	
	/*
	 * Suppression
	 */
	public void delete(Periodicite Periodicite) {
		try {
			
			PreparedStatement request = this.connection.prepareStatement("DELETE FROM Periodicite WHERE id_periodicite = ?", Statement.RETURN_GENERATED_KEYS);
			request.setInt(1, Periodicite.getId_periodicite());
			request.executeUpdate();
			
		} catch(SQLException error) {
			System.out.println("Erreur lors de la suppression : " + error.getMessage());
		}
	}
	
	/*
	 * Edition
	 */
	public void edit(Periodicite Periodicite) {
		try {
			PreparedStatement request = this.connection.prepareStatement("UPDATE Periodicite SET libelle = ? WHERE id_periodicite = ?", Statement.RETURN_GENERATED_KEYS);
			request.setString(1, Periodicite.getLibelle());
			request.setInt(2, Periodicite.getId_periodicite());
			request.executeUpdate();
		} catch(SQLException error) {
			System.out.println("Erreur lors de la modification : " + error.getMessage());
		}
	}
}
