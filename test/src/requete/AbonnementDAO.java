package requete;

import java.sql.SQLException;

import objets.Abonnement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;

public class AbonnementDAO {
	
	private Connection connection;
	
	public AbonnementDAO(Connection connection) {
		this.connection = connection;
	}
	
	/*
	 * Selection
	 */
	public String selectAll() {
		String result = "";
		try {
			
			PreparedStatement request = this.connection.prepareStatement("SELECT * FROM Abonnement");
			ResultSet res = request.executeQuery();
			
			while(res.next()) {
				result += "- Abonnement : \n\t Client : " + res.getInt("id_client") + " \n\t Revue : " + res.getString("id_revue") + 
						"\n\t Date d�but : " + res.getDate("date_debut").toLocalDate() + " \n\t Date fin : " + res.getDate("date_fin").toLocalDate() + "\n"; 
			}
			
			res.close();
			
			if (request != null) {
				request.close();
			}
			
			return result;
		} catch(SQLException error) {
			System.out.println("Erreur selection Abonnement : " + error.getMessage());
		}
		return "Erreur";
	}
	
	/**
	 * Cr�ation
	 */
	public void create(Abonnement Abonnement) {
		try {
			PreparedStatement request = this.connection.prepareStatement("INSERT INTO Abonnement (id_client, id_revue, date_debut, date_fin) VALUES (?, ?, ?, ?)");
			
			request.setInt(1, Abonnement.getId_client());
			request.setInt(2, Abonnement.getId_revue());
			request.setString(3, Abonnement.getDate_deb());
			request.setString(4, Abonnement.getDate_fin());
			
			request.executeUpdate();
		} catch (SQLException error) {
			System.out.println("Erreur cr�ation Abonnement : " + error.getMessage());
		}
	}
	
	/*
	 * Suppression
	 */
	public void delete(Abonnement Abonnement) {
		try {
			PreparedStatement request = this.connection.prepareStatement("DELETE FROM Abonnement WHERE id_client = ? AND id_revue = ?");
			request.setInt(1, Abonnement.getId_client());
			request.setInt(2, Abonnement.getId_revue());
			request.executeUpdate();
			
		} catch(SQLException error) {
			System.out.println("Erreur suppression Abonnement : " + error.getMessage());
		}
	}
	
	/*
	 * Edition
	 */
	public void edit(Abonnement Abonnement) {
		try {
			PreparedStatement request = this.connection.prepareStatement("UPDATE Abonnement SET date_debut = ?, date_fin = ? WHERE id_client = ? AND id_revue = ?");
			
			request.setString(1, Abonnement.getDate_deb());
			request.setString(2, Abonnement.getDate_fin());
			request.setInt(3, Abonnement.getId_client());
			request.setInt(4, Abonnement.getId_revue());
	
			request.executeUpdate();
		} catch(SQLException error) {
			System.out.println("Erreur modification Abonnement : " + error.getMessage());
		}
	}
	

}
