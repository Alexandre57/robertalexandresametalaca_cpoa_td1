package launcher;

import java.sql.Connection;
import java.util.Scanner;

import objets.*;
import requete.AbonnementDAO;
import requete.ClientDAO;
import requete.PeriodiciteDAO;
import requete.RevueDAO;

public class Menu {
	private Connection connection = null;
	private Scanner input = null;
	
	private int table = 0;
	private int action = 0;
	
	public Menu(Connection connection, Scanner input) {
		this.connection = connection;
		this.input = input;
	}
	
	public void init() {
		while(this.table == 0 && this.action == 0) {
			tableSelector();
		}
	}
	
	private void tableSelector() {
		System.out.println(
			"Quelle table souhaitez-vous modifier ?\n" +
			"\t (1) - Client\n" +
			"\t (2) - Revue\n" +
			"\t (3) - Abonnement\n" +
			"\t (4) - Periodicite\n" +
			"\t (q) - Quitter\n\n"
		);
		
		String s = this.input.nextLine();
		
		if(s.matches("q")) {
			System.exit(0);
		}
		
		try {
			this.table = Integer.parseInt(s);
		} catch(NumberFormatException error) {
			this.tableSelector();
			return;
		}
				
		this.actionSelector();
	}
	
	private void actionSelector() {
		System.out.println(
			"Quelle action souhaitez vous effectuer ?\n" +
			"\t (1) - Afficher\n" +
			"\t (2) - Cr�er\n" +
			"\t (3) - Modifier\n" +
			"\t (4) - Supprimer\n"+
			"\t (q) - Quitter\n\n"
		);
		
		String s = this.input.nextLine();
		
		if(s.matches("q")) {
			this.tableSelector();
			return;
		}
		
		try {
			this.action = Integer.parseInt(s);
		} catch(NumberFormatException error) {
			this.actionSelector();
			return;
		}
		this.actionHandler();
	}
	
	private void actionHandler() {
		switch(this.table) {
			case 1:
				clientHandler(this.action);
				break;
			case 2:
				revueHandler(this.action);
				break;
			case 3:
				abonnementHandler(this.action);
				break;
			case 4:
				periodiciteHandler(this.action);
				break;
		}
		this.actionSelector();
	}
	
	
	private void clientHandler(int action) {
		ClientDAO client = new ClientDAO(this.connection);
		switch(action) {
			case 1:
				System.out.println(client.selectAll());
				break;
			case 2:
				String nom, prenom, no_rue, voie, code_postal, ville, pays;
								
				System.out.println("Nom : ");
				nom = this.input.next();
				
				System.out.println("Pr�nom : ");
				prenom = this.input.next();
				
				System.out.println("N� Rue : ");
				no_rue = this.input.next();
				
				System.out.println("Voie : ");
				voie = this.input.next();
				
				System.out.println("Code postal : ");
				code_postal = this.input.next();
				
				System.out.println("Ville : ");
				ville = this.input.next();
				
				System.out.println("Pays : ");
				pays = this.input.next();
				
				Client cl = new Client(nom, prenom, no_rue, voie, code_postal, ville, pays);
				ClientDAO.create(cl);
				
				break;
			case 3:
				String nom2, prenom2, no_rue2, voie2, code_postal2, ville2, pays2;
				int id_client;
				
				System.out.println("ID Client : ");
				id_client = input.nextInt();
				
				System.out.println("Nom : ");
				nom2 = input.next();
				
				System.out.println("Pr�nom : ");
				prenom2 = input.next();
				
				System.out.println("N� Rue : ");
				no_rue2 = input.next();
				
				System.out.println("Voie : ");
				voie2= input.next();
				
				System.out.println("Code postal : ");
				code_postal2 = input.next();
				
				System.out.println("Ville : ");
				ville2 = input.next();
				
				System.out.println("Pays : ");
				pays2 = input.next();
				
				Client cl1 = new Client(id_client, nom2, prenom2, no_rue2, voie2, code_postal2, ville2, pays2);
				ClientDAO.edit(cl1);
				
				break;
			case 4:
				int id_client2;
				System.out.println("ID Client : ");
				id_client2 = input.nextInt();
				
				Client cl2 = new Client(id_client2);
				ClientDAO.delete(cl2);
				
				break;
		}
	}
	
	
	private void revueHandler(int action) {
		RevueDAO revue = new RevueDAO(this.connection);
		switch(action) {
			case 1:
				System.out.println(revue.selectAll());
				break;
			case 2:
				String titre, description, visuel;
				float tarif;
				int id_periodicite;
				
				System.out.print("\nTitre : ");
				titre = this.input.next();
				
				System.out.print("\nDescription : ");
				description = this.input.next();
				
				System.out.print("\nTarif : ");
				tarif = Float.parseFloat(this.input.next());
				
				System.out.print("\nVisuel : ");
				visuel = this.input.next();
				
				System.out.print("\nID Periodicite : ");
				id_periodicite = this.input.nextInt();
				
				Revue Rev = new Revue(titre,description,tarif,visuel,id_periodicite);
				RevueDAO.create(Rev);
				
				break;
			case 3:
				String titre2, description2, visuel2;
				float tarif2;
				int id_periodicite2, id;
				
				System.out.print("\nID Revue : ");
				id = this.input.nextInt();
				
				System.out.print("\nTitre : ");
				titre2 = this.input.next();
				
				System.out.print("\nDescription : ");
				description2 = this.input.next();
				
				System.out.print("\nTarif : ");
				tarif2 = this.input.nextFloat();
				
				System.out.print("\nVisuel : ");
				visuel2 = this.input.next();
				
				System.out.print("\nID Periodicite : ");
				id_periodicite2 = this.input.nextInt();
				
				Revue Rev2 = new Revue(id,titre2,description2,tarif2,visuel2,id_periodicite2);
				RevueDAO.edit(Rev2);
				break;
			case 4:
				int id2;
				System.out.print("\nID Revue : ");
				id2 = input.nextInt();
				
				Revue Rev3 = new Revue(id2);
				RevueDAO.delete(Rev3);
				break;
		}
	}
	
	private void abonnementHandler(int action) {
		AbonnementDAO AbonnementDAO = new AbonnementDAO(this.connection);
		switch(action) {
			case 1:
				System.out.println(AbonnementDAO.selectAll());
				break;
			case 2:
				int id_client, id_revue;
				String date_deb, date_fin;
				
				System.out.print("\nID Client : ");
				id_client = input.nextInt();
				
				System.out.print("\nID Revue : ");
				id_revue = input.nextInt();
				
				System.out.print("\nDate d�but (aaaa-mm-jj) : ");
				date_deb = input.next();
			
				System.out.print("\nDate fin (aaaa-mm-jj) : ");
				date_fin = input.next();
				
				Abonnement abo = new Abonnement(id_client, id_revue, date_deb, date_fin);
				AbonnementDAO.create(abo);
				
				break;
			case 3:
				int id_client2, id_revue2;
				String date_deb2, date_fin2;
				
				System.out.print("\nID Client : ");
				id_client2 = input.nextInt();
				
				System.out.print("\nID Revue : ");
				id_revue2 = input.nextInt();
				
				System.out.print("\nDate d�but (aaaa-mm-jj) : ");
				date_deb2 = input.next();
			
				System.out.print("\nDate fin (aaaa-mm-jj) : ");
				date_fin2 = input.next();
				
				Abonnement abo2 = new Abonnement(id_client2, id_revue2, date_deb2, date_fin2);
				AbonnementDAO.edit(abo2);

				break;
			case 4:
				int id3, id4;
				System.out.print("\nID Client : ");
				id3 = input.nextInt();
				
				System.out.print("\nID Revue : ");
				id4 = input.nextInt();
				
				Abonnement abo3 = new Abonnement(id3, id4);
				AbonnementDAO.delete(abo3);
				
				break;
		}
	}
	
	
	private void periodiciteHandler(int action) {
		PeriodiciteDAO PeriodiciteDAO = new PeriodiciteDAO(this.connection);
		switch(action) {
			case 1:
				System.out.println(PeriodiciteDAO.selectAll());
				break;
			case 2:
				String libelle;
				System.out.print("\nLibell� : ");
				libelle = input.next();
				
				Periodicite Perio1 = new Periodicite(libelle);			
				PeriodiciteDAO.create(Perio1);
				
				break;
			case 3:
				int id2;
				String libelle2;
				
				System.out.print("\nID Periodicite : ");
				id2 = input.nextInt();
				
				System.out.print("\nLibell� : ");
				libelle2 = input.next();
				
				Periodicite Perio2 = new Periodicite(id2,libelle2);			
				PeriodiciteDAO.edit(Perio2);
				
				break;				
			case 4:
				int id3;
				System.out.print("\nID Periodicite : ");
				id3 = input.nextInt();
				
				Periodicite Perio3 = new Periodicite(id3);			
				PeriodiciteDAO.delete(Perio3);
				
				break;
		}
	}

}
