package launcher;

import java.sql.Connection;
import java.util.Scanner;

import database.Database;

public class Launcher {
	
	public static void main(String[] args) {
		Connection connection = Database.getConnection("jdbc:mysql://infodb.iutmetz.univ-lorraine.fr:3306/mathis57u_infraction", "mathis57u_appli", "31511019");
		//Connection connection = Database.getConnection("jdbc:mysql://localhost:3306/cpoa", "root", "");
		Scanner input = new Scanner(System.in);
		Menu menu = new Menu(connection, input);
		menu.init();
	}
}
